<!DOCTYPE html>
<html>
<head>
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body class="w3-content" style="max-width:1300px">

<!-- First Grid: Logo & About -->
<div class="w3-row">
  <div class="w3-half w3-black w3-container w3-center" style="height:700px">
    <div class="w3-padding-64">
      <h1>My Logo</h1>
      <img src="/img/ff.jpeg" style="height:150px;width:30%">
    </div>
    <div class="w3-padding-64">
      <a href="#" class="w3-button w3-black w3-block w3-hover-blue-grey w3-padding-16">Home</a>
      <a href="#me" class="w3-button w3-black w3-block w3-hover-blue-grey w3-padding-16">About Me</a>
      <a href="#work" class="w3-button w3-black w3-block w3-hover-teal w3-padding-16">My Work</a>
      <a href="#edu" class="w3-button w3-black w3-block w3-hover-dark-grey w3-padding-16">Educational Background</a>
      <a href="#contact" class="w3-button w3-black w3-block w3-hover-brown w3-padding-16">Contact</a>
    </div>
  </div>
  <div class="w3-half w3-blue-grey w3-container" style="height:700px" id="me">
    <div class="w3-padding-64 w3-center">
      <h1>About Me</h1>
      <img src="/img/ff2.jpeg" style="height:150px;width:35%">
      <div class="w3-left-align w3-padding-large">
        <p>Lulusan SMK, dengan jurusan Teknik Komputer & Jaringan pada tahun 2018. Menjadi Mahasiswa di STMIK Widuri Jakarta dengan program studi Sistem Informasi.</p>
        <p>Memiliki pengalaman dan di akui berdasarkan sertifikasi Binus Center Merakit Personal Komputer dengan predikat Kompeten, Active Directory with Windows Server dengan predikat Kompeten, 
            Implementasi Mikrotik Tingkat Lanjut dengan predikat Sangat Kompeten. Dan pernah mengikuti training CCNA selama 2 bulan yang diselenggarakan oleh Nixtrain. 
            Mengikuti dan mendapatkan sertifikat IC3 Digital Literacy Certification pada tahun 2021.</p>
        <p>Perpengalaman menjadi IT Helpdesk selama 2 tahun dan di lanjutkan sebagai IT Programmar Junior di Perusahaan NSC Finance sampai sekarang.</p>
      </div>
    </div>
  </div>
</div>

<!-- Second Grid: Work & Educational Background -->
<div class="w3-row">
  <div class="w3-half w3-light-grey w3-center" style="min-height:800px" id="work">
    <div class="w3-padding-64">
      <h2>My Work</h2>
      <p>Some of my latest projects.</p>
    </div>
    <div class="w3-row">
      <div class="w3-half">
        <img src="/img/ff3.jpeg" style="height:400px;width:70%">
      </div>
      <div class="w3-half">
        <img src="/img/ff4.jpeg" style="height:400px;width:90%">
      </div>
    </div>
    <div class="w3-row w3-hide-small">
      <div class="w3-half">
       <p> Website : Nusantara-sakti.co.id </p>
      </div>
    </div><br>
  </div>
  <div class="w3-half w3-indigo w3-container" style="min-height:800px" id="edu">
    <div class="w3-padding-64 w3-center">
      <h2>Educational Background</h2>
      <p>A draft from my CV</p>
      <div class="w3-container w3-responsive">
        <table class="w3-table">
          <tr>
            <th>Year</th>
            <th>Title</th>
            <th>Where</th>
          </tr>
          <tr class="w3-white">
            <td>2006-2012</td>
            <td>Lulusan Sekolah Dasar </td>
            <td>SDN Palmerah 25 Pagi</td>
          </tr>
          <tr>
            <td>2012-2015</td>
            <td>Lulusan Madrasah</td>
            <td>MTsN 35 JAKARTA</td>
          </tr>
          <tr class="w3-white">
            <td>2015-2018</td>
            <td>Lulusan Teknik Komputer & Jaringan</td>
            <td>SMK Islam Assa'adatul Abadiyah</td>
          </tr>
          <tr>
            <td>2018</td>
            <td>Sertifikasi Merakit Personal Komputer</td>
            <td>BINUS CENTER, Jakarta</td>
          </tr>
          <tr class="w3-white">
            <td>2018</td>
            <td>Active Directory with Windows Server</td>
            <td>BINUS CENTER, Jakarta</td>
          </tr>
          <tr class="w3-hide-medium">
            <td>2018</td>
            <td>Implementasi Mikrotik Tingkat Lanjut</td>
            <td>BINUS CENTER, Jakarta</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Third Grid: Swing By & Contact -->
<div class="w3-row" id="contact">
  <div class="w3-half w3-dark-grey w3-container w3-center" style="height:700px">
    <div class="w3-padding-64">
      <h1>Swing By</h1>
    </div>
    <div class="w3-padding-64">
      <p>..for a cup of coffee, or whatever.</p>
      <p>Jakarta, Indonesia</p>
      <p>+62 881024923289</p>
      <p>farahfauziah57@gmail.com</p>
    </div>
  </div>
  <div class="w3-half w3-teal w3-container" style="height:700px">
    <div class="w3-padding-64 w3-padding-large">
      <h1>Contact</h1>
      <p class="w3-opacity">GET IN TOUCH</p>
      <form class="w3-container w3-card w3-padding-32 w3-white" action="/action_page.php" target="_blank">
        <div class="w3-section">
          <label>Name</label>
          <input class="w3-input" style="width:100%;" type="text" required name="Name">
        </div>
        <div class="w3-section">
          <label>Email</label>
          <input class="w3-input" style="width:100%;" type="text" required name="Email">
        </div>
        <div class="w3-section">
          <label>Message</label>
          <input class="w3-input" style="width:100%;" type="text" required name="Message">
        </div>
        <button type="submit" class="w3-button w3-teal w3-right">Send</button>
      </form>
    </div>
  </div>
</div>

<!-- Footer -->
<footer class="w3-container w3-black w3-padding-16">
  <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
</footer>

</body>
</html>